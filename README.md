# http2xmpp

Simple Web API to send xmpp messages via http post requests.

## Configuration
  - The xmpp sender and receivers are configured in the `config.ini` file.
  - Section ACCOUNT holds the jid (jabber identifier) and password of the user (bot) supposed to send messages.
  - Section RECEIVER holds a 64 character alpha-numeric bearer tokens used for authorziations. To each token exactly one receiver jid must be assigned.

### Get started
  - Clone the repo and navigate in the data directory: `cd data`
  - Copy the example configuration file: `cp config.ini.example config.ini`
  - Add some bearer tokens to the config files: `./getToken.py >> config.ini`
  - Setup the sender jid and password, delete the example RECEIVERS and update your own: `vim config.ini`
  - Start the container: `docker-compose up -d`

### Test 
```
curl -i http://localhost:8010 \
     -H "Authorization: Bearer 2vAWHgdTn74Rb2D4uU5IvyhSKF4xDYGVnu6D8syGU7Wixx4dXfmvxubCMwOxNKWQ" \
     -X POST -H "Content-Type: text/plain" \
     --data "put your message here"
```

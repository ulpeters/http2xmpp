#!/usr/bin/python3

import string
import secrets
alphanum = string.ascii_letters + string.digits
token = ''.join(secrets.choice(alphanum) for i in range(64))
print(token)
FROM ubuntu:focal

RUN \
  apt-get update && \
  apt-get install --yes \
    python3 \
    python3-pip \
    python3-venv \
    wget

RUN \
  python3 -m venv /opt/http2xmpp && \
  . /opt/http2xmpp/bin/activate && \
  pip install wheel && \
  pip install slixmpp

EXPOSE 8080
USER 1000

CMD /opt/http2xmpp/server/run.sh
